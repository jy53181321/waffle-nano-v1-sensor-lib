import utime
# part 1 : 所用音符对应赫兹
L3 = 659
L4 = 698
L5 = 784
L6 = 880
L7 = 988
M1 = 1047
M2 = 1175
M3 = 1319
M4 = 1397
M5 = 1568
M6 = 1760
M7 = 1976
H1 = 2093
H2 = 2349
H3 = 2637
H4 = 2794
H5 = 3136
H6 = 3520
H7 = 3951
sL2 = 622
sL4 = 740
sL5 = 831
sL6 = 932
sM1 = 1046
sM2 = 1245
sM4 = 1480
sM5 = 1661
sM6 = 1865
sH1 = 2218
sH2 = 2489
sH4 = 2960
sH5 = 3323
sH6 = 3729
# part 2 : 控制蜂鸣器发声函数
class NumberedMusicalNotation():
    def __init__(self, pwm):
        self.pwm = pwm

    def beep(self, freq, t_ms):
        flag = 0
        if freq == 0:
            flag = 1
            ranges = 1
        else:
            ranges = freq
        self.pwm.freq(ranges)
        if flag == 1:
            self.pwm.duty(99)
        else:
            self.pwm.duty(95)
        if t_ms > 0:
            utime.sleep(t_ms)
        self.pwm.freq(1)
        self.pwm.duty(99)
        utime.sleep(0.01)
        return
