from machine import I2C, Pin, SPI
from utime import sleep
import ds1307, st7789

i2c = I2C(1, sda=Pin(0), scl=Pin(1),freq=400000)
week_day = [" ", "Mon", "Tue", "Wed", "Thus", "Fri", "Sat", "Sun"]
print(i2c.scan())
ds = ds1307.DS1307(i2c)

spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.fill(st7789.color565(255, 255, 255))

while True:
    display.fill(st7789.color565(255, 255, 255))
    date = ds.get_time()
    year = date[0]
    month = date[1]
    day = date[2]
    weekday = date[3]
    hour = date[4]
    minute = date[5]
    second = date[6]

    pre_str = "%04d/%02d/%02d" % (year, month, day)
    post_str = "%02d:%02d:%02d" % (hour, minute, second)
    display.draw_string(45, 70, pre_str, size=3, color=st7789.color565(0, 0, 0))
    display.draw_string(60, 110, post_str, size=3, color=st7789.color565(0, 0, 0))
    display.draw_string(97, 150, week_day[weekday], size=3, color=st7789.color565(0, 0, 0))
    sleep(1)