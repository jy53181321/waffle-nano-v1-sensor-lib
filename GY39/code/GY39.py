#使用说明
'''
外部需要使用到Connect_Wifi，Display，Gy39三个函数和类
所有需要用到的参数在外部均设置了初始化设置
不建议改变大部分初始化参数，仅建议改变网络和MQTT相关参数

在连接MQTT时，如果屏幕停留在初始界面可能是因为WIFi连接出错
显示Reconnecting...是未能连入MQTT，请检查设置是否有问题
显示Has Connected表示连接成功进入程序

Gy39类提供接收发送MQTT的功能，可以通过以下指令进行操作
Prism、prism、PRISM（指令仅包括全部大写、小写或首字母大写，下同省略）：
可以选择屏幕镜像与否，默认为1需要使用棱镜进行观察
Change、Mode：
可以改变数据显示模式，默认为全部同时显示，还可以改变为滚动显示
At、Temp、Hum、Pa、Lux、High：
通过MQTT返回当前AT(体感温度)、Temp(实际温度)、Pa(压强)、Hum(湿度)、Lux(光照)、High(海拔)
End:
可以结束程序
'''
#--------------------------------------------------------------------------------
import utime,math,machine,network,st7789,utime
import ustruct as struct
from ubinascii import hexlify
from machine import Pin,UART,SPI
try:
    import usocket as socket
except:
    import socket
#引入相关库
#--------------------------------------------------------------------------------
def Get_Information(Mode,display,Size):
    uart = UART(1, baudrate=9600, bits=8, parity=0, rx=Pin(5), tx=Pin(0))
    uart.write('A5 83 28')
    utime.sleep_ms(1000)
    str1 = uart.read(9)
    str2 = uart.read(15)
    if(str1[8]==90 and str2[2]==4):
        str3=uart.read(8)
        str1 = uart.read(9)
        str2 = uart.read(15)
    #未知原因导致两次连发str1通过这个判断来调整
    Lux=str1[4]<<24|str1[5]<<16|str1[6]<<8|str1[7]
    Temp=str2[4]<<8|str2[5]
    Pa=str2[6]<<24|str2[7]<<16|str2[8]<<8|str2[9]
    Hum=str2[10]<<8|str2[11]
    High=str2[12]<<8|str2[13]
    lux=float(Lux/100)
    temp=float(Temp/100)
    pa=float(Pa/100)
    hum=float(Hum/100)
    high=float(High)
    e=(hum/100)*6.105*math.exp((17.27*temp/(237.7+temp)))
    v=0
    AT=1.07*temp+0.2*e-2.7-0.65*v
    at=str(AT)
    Temp=str(temp)
    Lux=str(lux)
    Pa=str(pa)
    Hum=str(hum)
    High=str(high)
    Mode_Chose(Mode,display,at,Temp,Lux,Pa,Hum,High,Size)
    return at,Temp,Lux,Pa,Hum,High
#GY39驱动并用于主程序中用于获取信息
#--------------------------------------------------------------------------------
class MQTTException(Exception):
    pass
class MQTTClient:
    def __init__(self, client_id, server, port=0, user=None, password=None, keepalive=0,
                 ssl=False, ssl_params={}):
        if port == 0:
            port = 8883 if ssl else 1883
        self.client_id = client_id
        self.sock = None
        self.server = server
        self.port = port
        self.ssl = ssl
        self.ssl_params = ssl_params
        self.pid = 0
        self.cb = None
        self.user = user
        self.pswd = password
        self.keepalive = keepalive
        self.lw_topic = None
        self.lw_msg = None
        self.lw_qos = 0
        self.lw_retain = False
    def _send_str(self, s):
        self.sock.write(struct.pack("!H", len(s)))
        self.sock.write(s)
    def _recv_len(self):
        n = 0
        sh = 0
        while 1:
            b = self.sock.read(1)[0]
            n |= (b & 0x7f) << sh
            if not b & 0x80:
                return n
            sh += 7
    def set_callback(self, f):
        self.cb = f
    def set_last_will(self, topic, msg, retain=False, qos=0):
        assert 0 <= qos <= 2
        assert topic
        self.lw_topic = topic
        self.lw_msg = msg
        self.lw_qos = qos
        self.lw_retain = retain
    def connect(self, clean_session=True):
        self.sock = socket.socket()
        addr = socket.getaddrinfo(self.server, self.port)[0][-1]
        self.sock.connect(addr)
        if self.ssl:
            import ussl
            self.sock = ussl.wrap_socket(self.sock, **self.ssl_params)
        premsg = bytearray(b"\x10\0\0\0\0\0")
        msg = bytearray(b"\x04MQTT\x04\x02\0\0")
        sz = 10 + 2 + len(self.client_id)
        msg[6] = clean_session << 1
        if self.user is not None:
            sz += 2 + len(self.user) + 2 + len(self.pswd)
            msg[6] |= 0xC0
        if self.keepalive:
            assert self.keepalive < 65536
            msg[7] |= self.keepalive >> 8
            msg[8] |= self.keepalive & 0x00FF
        if self.lw_topic:
            sz += 2 + len(self.lw_topic) + 2 + len(self.lw_msg)
            msg[6] |= 0x4 | (self.lw_qos & 0x1) << 3 | (self.lw_qos & 0x2) << 3
            msg[6] |= self.lw_retain << 5
        i = 1
        while sz > 0x7f:
            premsg[i] = (sz & 0x7f) | 0x80
            sz >>= 7
            i += 1
        premsg[i] = sz
        self.sock.write(premsg, i + 2)
        self.sock.write(msg)
        self._send_str(self.client_id)
        if self.lw_topic:
            self._send_str(self.lw_topic)
            self._send_str(self.lw_msg)
        if self.user is not None:
            self._send_str(self.user)
            self._send_str(self.pswd)
        resp = self.sock.read(4)
        assert resp[0] == 0x20 and resp[1] == 0x02
        if resp[3] != 0:
            raise MQTTException(resp[3])
        return resp[2] & 1
    def disconnect(self):
        self.sock.write(b"\xe0\0")
        self.sock.close()
    def ping(self):
        self.sock.write(b"\xc0\0")
    def publish(self, topic, msg, retain=False, qos=0):
        pkt = bytearray(b"\x30\0\0\0")
        pkt[0] |= qos << 1 | retain
        sz = 2 + len(topic) + len(msg)
        if qos > 0:
            sz += 2
        assert sz < 2097152
        i = 1
        while sz > 0x7f:
            pkt[i] = (sz & 0x7f) | 0x80
            sz >>= 7
            i += 1
        pkt[i] = sz
        #print(hex(len(pkt)), hexlify(pkt, ":"))
        self.sock.write(pkt, i + 1)
        self._send_str(topic)
        if qos > 0:
            self.pid += 1
            pid = self.pid
            struct.pack_into("!H", pkt, 0, pid)
            self.sock.write(pkt, 2)
        self.sock.write(msg)
        if qos == 1:
            while 1:
                op = self.wait_msg()
                if op == 0x40:
                    sz = self.sock.read(1)
                    assert sz == b"\x02"
                    rcv_pid = self.sock.read(2)
                    rcv_pid = rcv_pid[0] << 8 | rcv_pid[1]
                    if pid == rcv_pid:
                        return
        elif qos == 2:
            assert 0
    def subscribe(self, topic, qos=0):
        assert self.cb is not None, "Subscribe callback is not set"
        pkt = bytearray(b"\x82\0\0\0")
        self.pid += 1
        struct.pack_into("!BH", pkt, 1, 2 + 2 + len(topic) + 1, self.pid)
        #print(hex(len(pkt)), hexlify(pkt, ":"))
        self.sock.write(pkt)
        self._send_str(topic)
        self.sock.write(qos.to_bytes(1, "little"))
        while 1:
            op = self.wait_msg()
            if op == 0x90:
                resp = self.sock.read(4)
                #print(resp)
                assert resp[1] == pkt[2] and resp[2] == pkt[3]
                if resp[3] == 0x80:
                    raise MQTTException(resp[3])
                return
    def wait_msg(self):
        res = self.sock.read(1)
        self.sock.setblocking(True)
        if res is None:
            return None
        if res == b"":
            raise OSError(-1)
        if res == b"\xd0":  # PINGRESP
            sz = self.sock.read(1)[0]
            assert sz == 0
            return None
        op = res[0]
        if op & 0xf0 != 0x30:
            return op
        sz = self._recv_len()
        topic_len = self.sock.read(2)
        topic_len = (topic_len[0] << 8) | topic_len[1]
        topic = self.sock.read(topic_len)
        sz -= topic_len + 2
        if op & 6:
            pid = self.sock.read(2)
            pid = pid[0] << 8 | pid[1]
            sz -= 2
        msg = self.sock.read(sz)
        self.cb(msg)
        if op & 6 == 2:
            pkt = bytearray(b"\x40\x02\0\0")
            struct.pack_into("!H", pkt, 2, pid)
            self.sock.write(pkt)
        elif op & 6 == 4:
            assert 0
    def check_msg(self):
        self.sock.setblocking(False)
        return self.wait_msg()
#MQTT需要类
#--------------------------------------------------------------------------------
def Connect_Wifi(Username,Password):
    wl = network.WLAN()
    wl.active(1)
    wl.connect(Username,Password,security=network.AUTH_PSK) #第一栏为wifi名第二栏为密码
#连接WiFi
#--------------------------------------------------------------------------------
def Display(Prism):
    spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
    if Prism==1:
        display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT),direction=st7789.Y_MIRROR)
    else:
        display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
    display.init()
    display.draw_string(50, 230, "Powered by BlackWalnuut Labs.")
    return display
#屏幕初始化
#--------------------------------------------------------------------------------
def Mode_Chose(Mode,display,at,Temp,Lux,Pa,Hum,High,Size):
    if Mode==1:    
        display.fill(st7789.color565(0, 0, 0))
        display.draw_string(10, 10,"Your Information:" ,size=2,color=st7789.color565(255, 255, 255))
        display.draw_string(10, 40,"AT:"+str(at[0:5] ),size=Size,color=st7789.color565(255, 255, 255))
        display.draw_string(10, 70,"Temp:"+str(Temp[0:5]) ,size=Size,color=st7789.color565(255, 255, 255))
        display.draw_string(10, 100,"Hum:"+Hum[0:5]+"%" ,size=Size,color=st7789.color565(255, 255, 255))
        display.draw_string(10, 130,"Pa:"+Pa[0:5] ,size=Size,color=st7789.color565(255, 255, 255))
        display.draw_string(10, 160,"Lux:"+Lux[0:5] ,size=Size,color=st7789.color565(255, 255, 255))
        display.draw_string(10, 190,"High:"+High[0:5] ,size=Size,color=st7789.color565(255, 255, 255))
    elif Mode==2:
        i=0
        Information="           AT:"+str(at[0:5])+"       Temp:"+Temp[0:5]+"       Hum:"+Hum[0:5]+"%      Pa:"+Pa[0:5]+"        Lux:"+Lux[0:5]+"       High:"+High[0:5]+"           end"
        while Information[i+10:]!='end':
            display.fill(st7789.color565(0, 0, 0))
            display.draw_string(0, 100, str(Information[i:i+9]),size=Size+2,color=st7789.color565(255, 255, 255))
            i+=1
            utime.sleep_ms(100)
#不同模式的展示方式
#--------------------------------------------------------------------------------


class Gy39():
    def __init__(self,display,Mode,Size,Prism,client_id,mqtt_server,Topic,mqtt_user,mqtt_password,mymsg=None,client=False):
        self.client_id = client_id #任意设置id
        self.mqtt_server = mqtt_server #测试用可改变
        self.port=1883
        self.Topic = Topic 
        self.mqtt_user = mqtt_user
        self.mqtt_password = mqtt_password
        self.mymsg=None
        self.display=display
        self.client = MQTTClient(self.client_id, self.mqtt_server, user=self.mqtt_user, password=self.mqtt_password,port=self.port)
        self.Mode=Mode
        self.Size=Size
        self.Prism=Prism
    def sub_cb(self,msg):  
        self.mymsg=msg
    def connect_and_subscribe(self):
        self.client.set_callback(self.sub_cb)
        self.client.connect()
        self.client.subscribe(self.Topic)
        self.display.fill(st7789.color565(0, 0, 0))
        self.display.draw_string(10, 100,"Has Connected" ,size=3,color=st7789.color565(255, 255, 255))
    def restart_and_reconnect(self):
        self.display.fill(st7789.color565(0, 0, 0))
        self.display.draw_string(10, 100,"Reconnecting..." ,size=3,color=st7789.color565(255, 255, 255))
        utime.sleep(10)
        machine.reset()
    def sub_and_pub(self):
        at,Temp,Lux,Pa,Hum,High=Get_Information(self.Mode,self.display,self.Size)
        self.client.subscribe(self.Topic)
        if(self.mymsg!=None):
            Mymsg=str(self.mymsg,'utf-8')  
            if Mymsg=="AT"or Mymsg=="at"or Mymsg=="At":
                self.client.publish(self.Topic, "AT: "+at[0:5],)
            elif Mymsg=="temp"or Mymsg=="Temp"or Mymsg=="TEMP":
                self.client.publish(self.Topic, "Temp: "+Temp[0:5],)
            elif Mymsg=="hum"or Mymsg=="Hum"or Mymsg=="HUM":
                self.client.publish(self.Topic, "Hum: "+Hum[0:5],)
            elif Mymsg=="pa"or Mymsg=="PA"or Mymsg=="Pa":
                self.client.publish(self.Topic, "Pa: "+Pa[0:5],)
            elif Mymsg=="lux"or Mymsg=="LUX"or Mymsg=="Lux":
                self.client.publish(self.Topic, "Lux: "+Lux[0:5],)
            elif Mymsg=="high"or Mymsg=="High"or Mymsg=="HIGH":
                self.client.publish(self.Topic, "High: "+High[0:5],)
            elif Mymsg=="change"or Mymsg=="CHANGE"or Mymsg=="Change"or Mymsg=="MODE"or Mymsg=="mode"or Mymsg=="Mode":
                self.client.publish(self.Topic, "Mode has Changed",)    
                if self.Mode==1:
                    self.Mode=2
                elif self.Mode==2:
                    self.Mode=1        
            elif Mymsg=="close"or Mymsg=="end"or Mymsg=="Close"or Mymsg=="End"or Mymsg=="CLOSE"or Mymsg=="END":
                self.client.publish(self.Topic, "ByeBye!",)
                self.display.fill(st7789.color565(0, 0, 0))
                self.display.draw_string(10, 100, "ByeBye!",size=6,color=st7789.color565(255, 255, 255))
                utime.sleep(2)
                spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
                if self.Prism==1:
                    self.display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT),direction=st7789.Y_MIRROR)
                else:
                    self.display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
                self.display.init()
                self.display.draw_string(50, 230, "Powered by BlackWalnuut Labs.")
                utime.sleep(3)
                Break=1
                return Break
            elif Mymsg=="prism"or Mymsg=="Prism"or Mymsg=="PRISM":
                self.client.publish(self.Topic, "Prism has Changed",)    
                spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
                if self.Prism==1:
                    self.Prism=0
                    self.display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
                elif self.Prism==0:
                    self.Prism=1
                    self.display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT),direction=st7789.Y_MIRROR)
                self.display.init()
            elif Mymsg!=None :  
                self.client.publish(self.Topic, "Input Error!",)
            utime.sleep(1)
            self.client.subscribe(self.Topic)
            self.mymsg=None
            Mymsg=None      
#主程序中用于收发MQTT信息
#--------------------------------------------------------------------------------