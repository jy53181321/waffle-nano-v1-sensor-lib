from machine import SPI,Pin
import st7789,DS1302
ds=DS1302.DS1302(Pin(0),Pin(2),Pin(1))              #初始化DS1302的对象ds（括号中分别是DS1302上clk，dat，rst引脚对应的Waffle Nano上应该与之连接的引脚，理论上来说是只要Nano上未被占用的具有输入输出功能的引脚都能与clk，dat，rst连接，但推荐使用main函数中的配置）
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()                              #初始化屏幕
display.fill(st7789.color565(255, 255, 255))              #先将全屏清空
display.draw_string(10, 10,str(ds.DateTime()),size=1,color=st7789.color565(0,0,0),bg=st7789.WHITE)     #在屏幕上显示完整的时间（年，月，日，星期几，小时，分钟，秒这七者组成的列表）
display.draw_string(10, 30,str(ds.Year()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)         #显示年份数
display.draw_string(10, 50,str(ds.Month()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)         #显示月份数
display.draw_string(10, 70,str(ds.Day()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)           #显示日数
display.draw_string(10, 90,str(ds.Weekday()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)       #显示今天是星期几
display.draw_string(10, 110,str(ds.Hour()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)         #显示现在是几点
display.draw_string(10, 130,str(ds.Minute()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)       #显示现在是几分
display.draw_string(10, 150,str(ds.Second()),size=2,color=st7789.color565(0,0,0),bg=st7789.WHITE)       #显示现在是几秒