def temp():
    from machine import Pin, ADC   
    adc = ADC(Pin(13))              ##从GPIO13接口处读取数据
    adc.equ(ADC.EQU_MODEL_8)        ##平均采样8次
    a = "%.3g" % ((adc.read() * 0.488 - 32) / 1.8 )      ##计算公式
    return a