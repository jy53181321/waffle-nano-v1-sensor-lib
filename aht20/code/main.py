from machine import I2C, Pin, SPI
import utime
import ahtx0
import st7789
import ujson
import urandom

spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))

display.init()
utime.sleep(4)
display.fill(st7789.color565(255, 255, 255))
display.pixel(5, 5, st7789.color565(255, 0, 0))
display.draw_string(15, 55, "Welcome to use ",size=3,color=st7789.color565(0, 0, 0))
display.draw_string(5, 85, "the temperature",size=3,color=st7789.color565(0, 0, 0))
display.draw_string(30, 115, "and humidity",size=3,color=st7789.color565(0, 0, 0))
display.draw_string(60, 145, "detector",size=3,color=st7789.color565(0, 0, 0))
utime.sleep(4)

i2c=I2C(1,sda=Pin(0),scl=Pin(1),freq=100000)
print(i2c.scan())
sensor = ahtx0.AHT20(i2c)

while True:
    
    x=int(sensor.temperature*100)
    a=int(x/100)
    b=int(x%100)
    temp=float(x*1.0/100)
    print(temp)

    
    display.fill(st7789.color565(255, 255, 255))
    display.pixel(5, 5, st7789.color565(255, 0, 0))
    if(temp<-40):
        display.draw_string(1, 10, "Error",size=3,color=st7789.color565(0, 0, 0))
        display.draw_string(1, 40, "Check the connection",size=2,color=st7789.color565(0, 0, 0))
    elif(temp<0):
        b=100-b
        display.draw_string(20, 10, "Temperature:",size=3,color=st7789.color565(0, 0, 0))
        display.draw_string(30, 40, "  "+str(a)+"."+str(b)+" C",size=3,color=st7789.color565(0, 0, 0))
    else:
        display.draw_string(20, 10, "Temperature:",size=3,color=st7789.color565(0, 0, 0))
        display.draw_string(30, 40, "  "+str(a)+"."+str(b)+" C",size=3,color=st7789.color565(0, 0, 0))
    
    if(temp<12 and temp>=-40):
        display.draw_string(1, 70, "You need to make the te-",size=2,color=st7789.color565(0, 0, 0))
        display.draw_string(20, 90, "mperature higher.",size=2,color=st7789.color565(0, 0, 0))
    elif(temp>45):
        display.draw_string(1, 70, "You need to get the tem-",size=2,color=st7789.color565(0, 0, 0))
        display.draw_string(40, 90, "perature down.",size=2,color=st7789.color565(0, 0, 0))
    elif(temp>=12 and temp<=45):
        display.draw_string(5, 70, "The temperature at the ",size=2,color=st7789.color565(0, 0, 0))
        display.draw_string(45, 90, "right moment.",size=2,color=st7789.color565(0, 0, 0))


    x=int(sensor.relative_humidity*100)
    a=int(x/100)
    b=int(x%100)
    humi=float(x*1.0/100)
    print(humi)
    
    if(humi>80 or humi<0):
        display.draw_string(1, 120, "Error",size=3,color=st7789.color565(0, 0, 0))
        display.draw_string(1, 150, "Check the connection",size=2,color=st7789.color565(0, 0, 0))
    elif(humi<=80 and humi>=0):
        display.draw_string(45, 120, "Humidity:",size=3,color=st7789.color565(0, 0, 0))
        display.draw_string(30, 150, "  "+str(a)+"."+str(b)+" %",size=3,color=st7789.color565(0, 0, 0))
    
    if(humi<50):
        display.draw_string(1, 180, "You need to make the hu-",size=2,color=st7789.color565(0, 0, 0))
        display.draw_string(50, 200, "midity higher.",size=2,color=st7789.color565(0, 0, 0))
    elif(humi>90):
        display.draw_string(1, 180, "You need to get the hum-",size=2,color=st7789.color565(0, 0, 0))
        display.draw_string(40, 200, "idity down.",size=2,color=st7789.color565(0, 0, 0))
    elif(humi>=50 and humi<=90):
        display.draw_string(20, 180, "The humidity at the ",size=2,color=st7789.color565(0, 0, 0))
        display.draw_string(45, 200, "right moment.",size=2,color=st7789.color565(0, 0, 0))


    utime.sleep(4)
